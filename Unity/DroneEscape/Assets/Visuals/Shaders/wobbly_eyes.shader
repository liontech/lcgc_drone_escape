// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32537,y:32711,varname:node_3138,prsc:2|emission-2549-OUT;n:type:ShaderForge.SFN_Tex2d,id:9989,x:31796,y:32619,ptovrint:False,ptlb:Happy,ptin:_Happy,varname:node_9989,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:7930f0e34c2a7864caed1d0c8d454cfd,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:6205,x:31796,y:32943,ptovrint:False,ptlb:Neutral,ptin:_Neutral,varname:node_6205,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a8ac92095221fc64984198910110e2ff,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:34,x:31796,y:33262,ptovrint:False,ptlb:Bored,ptin:_Bored,varname:node_34,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:d4d31a9617b74b340a63856a16a1cad3,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ToggleProperty,id:428,x:31624,y:32964,ptovrint:False,ptlb:tHappy,ptin:_tHappy,varname:node_428,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_Add,id:9813,x:31796,y:32779,varname:node_9813,prsc:2|A-428-OUT,B-6999-OUT;n:type:ShaderForge.SFN_Multiply,id:1868,x:32008,y:32730,varname:node_1868,prsc:2|A-9989-RGB,B-9813-OUT;n:type:ShaderForge.SFN_Add,id:8119,x:31796,y:33098,varname:node_8119,prsc:2|A-2534-OUT,B-6999-OUT;n:type:ShaderForge.SFN_ToggleProperty,id:2534,x:31624,y:33055,ptovrint:False,ptlb:tNeutral,ptin:_tNeutral,varname:_tHappy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_Vector1,id:6999,x:31624,y:33294,varname:node_6999,prsc:2,v1:0;n:type:ShaderForge.SFN_Multiply,id:1075,x:32008,y:32980,varname:node_1075,prsc:2|A-6205-RGB,B-8119-OUT;n:type:ShaderForge.SFN_Add,id:7503,x:31796,y:33429,varname:node_7503,prsc:2|A-6099-OUT,B-6999-OUT;n:type:ShaderForge.SFN_ToggleProperty,id:6099,x:31624,y:33142,ptovrint:False,ptlb:tBored,ptin:_tBored,varname:_tHappy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_Multiply,id:8558,x:32023,y:33320,varname:node_8558,prsc:2|A-34-RGB,B-7503-OUT;n:type:ShaderForge.SFN_Add,id:2549,x:32356,y:32810,varname:node_2549,prsc:2|A-1868-OUT,B-1075-OUT,C-8558-OUT,D-237-OUT;n:type:ShaderForge.SFN_ToggleProperty,id:5029,x:31624,y:33232,ptovrint:False,ptlb:tStandby,ptin:_tStandby,varname:_tBored_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_Tex2d,id:3525,x:31795,y:33591,ptovrint:False,ptlb:Standby,ptin:_Standby,varname:_Bored_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:d4d31a9617b74b340a63856a16a1cad3,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:2326,x:31795,y:33758,varname:node_2326,prsc:2|A-5029-OUT,B-6999-OUT;n:type:ShaderForge.SFN_Multiply,id:237,x:32022,y:33649,varname:node_237,prsc:2|A-3525-RGB,B-2326-OUT;proporder:9989-428-6205-2534-34-6099-3525-5029;pass:END;sub:END;*/

Shader "Craft/wobbly_eyes" {
    Properties {
        _Happy ("Happy", 2D) = "white" {}
        [MaterialToggle] _tHappy ("tHappy", Float ) = 0
        _Neutral ("Neutral", 2D) = "white" {}
        [MaterialToggle] _tNeutral ("tNeutral", Float ) = 0
        _Bored ("Bored", 2D) = "white" {}
        [MaterialToggle] _tBored ("tBored", Float ) = 0
        _Standby ("Standby", 2D) = "white" {}
        [MaterialToggle] _tStandby ("tStandby", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Happy; uniform float4 _Happy_ST;
            uniform sampler2D _Neutral; uniform float4 _Neutral_ST;
            uniform sampler2D _Bored; uniform float4 _Bored_ST;
            uniform fixed _tHappy;
            uniform fixed _tNeutral;
            uniform fixed _tBored;
            uniform fixed _tStandby;
            uniform sampler2D _Standby; uniform float4 _Standby_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 _Happy_var = tex2D(_Happy,TRANSFORM_TEX(i.uv0, _Happy));
                float node_6999 = 0.0;
                float3 node_1868 = (_Happy_var.rgb*(_tHappy+node_6999));
                float4 _Neutral_var = tex2D(_Neutral,TRANSFORM_TEX(i.uv0, _Neutral));
                float3 node_1075 = (_Neutral_var.rgb*(_tNeutral+node_6999));
                float4 _Bored_var = tex2D(_Bored,TRANSFORM_TEX(i.uv0, _Bored));
                float3 node_8558 = (_Bored_var.rgb*(_tBored+node_6999));
                float4 _Standby_var = tex2D(_Standby,TRANSFORM_TEX(i.uv0, _Standby));
                float3 node_2549 = (node_1868+node_1075+node_8558+(_Standby_var.rgb*(_tStandby+node_6999)));
                float3 emissive = node_2549;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
