// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True;n:type:ShaderForge.SFN_Final,id:4795,x:32895,y:32714,varname:node_4795,prsc:2|emission-8798-OUT;n:type:ShaderForge.SFN_VertexColor,id:2053,x:32255,y:32869,varname:node_2053,prsc:2;n:type:ShaderForge.SFN_Color,id:797,x:31912,y:32642,ptovrint:True,ptlb:Color 2,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:8798,x:32688,y:32814,varname:node_8798,prsc:2|A-7120-OUT,B-7412-OUT;n:type:ShaderForge.SFN_DepthBlend,id:9706,x:32255,y:33015,varname:node_9706,prsc:2|DIST-2996-OUT;n:type:ShaderForge.SFN_Multiply,id:7412,x:32483,y:32926,varname:node_7412,prsc:2|A-2053-RGB,B-9706-OUT;n:type:ShaderForge.SFN_Time,id:7659,x:31577,y:32861,varname:node_7659,prsc:2;n:type:ShaderForge.SFN_Vector1,id:4925,x:31577,y:32790,varname:node_4925,prsc:2,v1:2;n:type:ShaderForge.SFN_Slider,id:2996,x:31912,y:33017,ptovrint:False,ptlb:Depth Blend,ptin:_DepthBlend,varname:node_2996,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.1;n:type:ShaderForge.SFN_Multiply,id:9695,x:31740,y:32790,varname:node_9695,prsc:2|A-4925-OUT,B-7659-TTR;n:type:ShaderForge.SFN_Sin,id:7968,x:31912,y:32790,varname:node_7968,prsc:2|IN-9695-OUT;n:type:ShaderForge.SFN_RemapRange,id:7477,x:32069,y:32790,varname:node_7477,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-7968-OUT;n:type:ShaderForge.SFN_Lerp,id:7120,x:32255,y:32723,varname:node_7120,prsc:2|A-48-RGB,B-797-RGB,T-7477-OUT;n:type:ShaderForge.SFN_Color,id:48,x:31912,y:32469,ptovrint:False,ptlb:Color 1,ptin:_Color1,varname:node_48,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;proporder:48-797-2996;pass:END;sub:END;*/

Shader "Craft/wobbly_glow" {
    Properties {
        _Color1 ("Color 1", Color) = (0.5,0.5,0.5,1)
        _TintColor ("Color 2", Color) = (0.5,0.5,0.5,1)
        _DepthBlend ("Depth Blend", Range(0, 0.1)) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _TintColor;
            uniform float _DepthBlend;
            uniform float4 _Color1;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 vertexColor : COLOR;
                float4 projPos : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
////// Lighting:
////// Emissive:
                float4 node_7659 = _Time + _TimeEditor;
                float3 emissive = (lerp(_Color1.rgb,_TintColor.rgb,(sin((2.0*node_7659.a))*0.5+0.5))*(i.vertexColor.rgb*saturate((sceneZ-partZ)/_DepthBlend)));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
