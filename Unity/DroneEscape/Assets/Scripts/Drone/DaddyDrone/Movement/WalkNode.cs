﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WalkNode : MonoBehaviour {

    [Serializable]
    public class Path {
        public WalkNode Destination;
        public List<WalkNode> Route;

        public float TotalDistance;
        public List<float> Distances;
    }

    public List<WalkNode> AccessibleNodes = new List<WalkNode>();
    public List<Path> Paths;

}
