﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(WalkNode), true)]
public class WalkNodeEditor : Editor {
    
    WalkNode myTarget;

    public override void OnInspectorGUI() {
        WalkNetwork network = myTarget.GetComponentInParent<WalkNetwork>();
        if (network == null) {
            GUILayout.Label("Can't find WalkNetwork in parents!");
            return;
        }

        GUIStyle style = new GUIStyle();
        style.fontSize = 20;
        style.alignment = TextAnchor.MiddleCenter;
        style.margin = new RectOffset(0, 0, 10, 0);
        GUILayout.Label(myTarget.name, style);

        GUILayout.Space(10.0f);

        foreach (WalkNode node in network.Nodes) {
            if (node == myTarget) { continue; }
            GUILayout.BeginHorizontal();

            GUILayout.Label(node.name);

            bool isAccessible = node.AccessibleNodes.Contains(myTarget);
            GUI.color = isAccessible ? Color.green : Color.white;

            if (GUILayout.Button(isAccessible ? "Accessible" : "Blocked")) {
                if (isAccessible) {
                    node.AccessibleNodes.RemoveAll(x => x == myTarget);
                    myTarget.AccessibleNodes.RemoveAll(x => x == node);
                } else {
                    node.AccessibleNodes.AddDistinct(myTarget);
                    myTarget.AccessibleNodes.AddDistinct(node);
                }

                network.UpdateNodes();
                network.Nodes.ForEach(x => EditorUtility.SetDirty(x));
            }

            GUILayout.EndHorizontal();

            GUILayout.Space(10.0f);
        }
        GUI.color = Color.white;
    }

    private void Awake() {
        myTarget = target as WalkNode;
    }

    private void OnSceneGUI() {
        WalkNetwork network = myTarget.GetComponentInParent<WalkNetwork>();
        if (network == null) { return; }
        WalkNetworkEditor.DrawWalkNodeNetwork(network.Nodes, myTarget);
    }

}
