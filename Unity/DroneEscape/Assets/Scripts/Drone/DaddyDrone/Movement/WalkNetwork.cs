﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WalkNetwork : MonoBehaviour {

    public List<WalkNode> Nodes { get { return nodes; } }

    [SerializeField] List<WalkNode> nodes;

    public void UpdateNodes() {
        nodes = new List<WalkNode>(GetComponentsInChildren<WalkNode>());
        UpdateWalkNodePaths();
    }

    private void UpdateWalkNodePaths() {
        Nodes.ForEach(x => x.Paths = new List<WalkNode.Path>());
        Nodes.ForEach(x => x.AccessibleNodes.RemoveAll(y => y == null));

        foreach (WalkNode node in Nodes) {

            foreach (WalkNode destination in Nodes) {
                if (node == destination) { continue; }

                List<List<WalkNode>> routes = new List<List<WalkNode>>();

                Action<WalkNode, List<WalkNode>> traverseNode = null; ;
                traverseNode = delegate (WalkNode currentNode, List<WalkNode> passedNodes) {
                    List<WalkNode> route = new List<WalkNode>(passedNodes);
                    route.Add(currentNode);
                    if (currentNode.AccessibleNodes.Contains(destination)) {
                        route.Add(destination);
                        routes.Add(route);
                        return;
                    }
                    foreach (WalkNode accessibleNode in currentNode.AccessibleNodes) {
                        if (route.Contains(accessibleNode)) { continue; }
                        traverseNode(accessibleNode, route);
                    }
                };

                traverseNode(node, new List<WalkNode>());

                if (routes.Count == 0) { continue; }

                float shortestDistance = float.MaxValue;
                List<WalkNode> shortestRoute = null;
                List<float> shortestDistances = null;
                foreach (List<WalkNode> route in routes) {
                    float totalDistance = 0.0f;
                    List<float> distances = new List<float>();
                    for (int i = 1; i < route.Count; i++) {
                        float distance = Vector3.Distance(route[i - 1].transform.position,
                                                          route[i].transform.position);
                        distances.Add(distance);
                        totalDistance += distance;
                    }
                    if (totalDistance < shortestDistance) {
                        shortestRoute = route;
                        shortestDistance = totalDistance;
                        shortestDistances = distances;
                    }
                }

                WalkNode.Path path = new WalkNode.Path();
                path.Destination = destination;
                path.Route = shortestRoute;
                path.TotalDistance = shortestDistance;
                path.Distances = shortestDistances;
                node.Paths.Add(path);

            }

        }
    }

}
