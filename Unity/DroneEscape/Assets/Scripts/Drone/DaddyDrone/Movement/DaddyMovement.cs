﻿using UnityEngine;
using System.Collections;
using System;

public class DaddyMovement : MonoBehaviour {

    [SerializeField] private Walker walkerPrefab;
    [SerializeField] private Rigidbody myRigidbody;
    [SerializeField] private float maxWalkerDistance;
    [SerializeField] private float force;
    [SerializeField] private float maxSpeed;

    private bool isInitialized;
    private Walker walker;

    public void WalkTo(WalkNode node, Action onDestination) {
        InitializeIfNeeded();
        walker.WalkToNode(node, onDestination);
    }

    private void Awake() {
        InitializeIfNeeded();
    }

    private void InitializeIfNeeded() {
        if (isInitialized) { return;}
        isInitialized = true;

        walker = Instantiate<Walker>(walkerPrefab);
        walker.transform.position = transform.position;
    }

    private void FixedUpdate() {
        Vector3 targetPosition = walker.transform.position;
        Vector3 deltaPosition = targetPosition - transform.position;
        if (myRigidbody.velocity.magnitude < maxSpeed) {
            myRigidbody.AddForce(deltaPosition * force);
        }

        if (Vector3.Distance(targetPosition, transform.position) > maxWalkerDistance) {
            walker.Pause();
        } else {
            walker.Continue();
        }
    }

}
