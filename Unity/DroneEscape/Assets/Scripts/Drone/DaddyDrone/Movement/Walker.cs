﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Walker : MonoBehaviour {

    public float Speed;

    private WalkNode currentNode;
    private bool isPaused = false;

    public void PositionAtNode(WalkNode node) {
        transform.localPosition = node.transform.localPosition;
        currentNode = node;
    }

    public void Pause() {
        isPaused = true;
    }

    public void Continue() {
        isPaused = false;
    }

    public void WalkToNode(WalkNode destination, Action onReached) {
        if (currentNode == null) {
            currentNode = destination;
        }

        if (currentNode == destination) {
            transform.localPosition = destination.transform.localPosition;
            if (onReached != null) {
                onReached();
            }
            return;
        }

        StopCoroutine("WalkToNodeOverTime");
        StartCoroutine("WalkToNodeOverTime", new object[] { destination, onReached });
    }

    private IEnumerator WalkToNodeOverTime(object[] parameters) {
        WalkNode destination = parameters[0] as WalkNode;
        Action onReached = parameters[1] as Action;

        float timeSinceStart = 0.0f;
        
        WalkNode.Path path = currentNode.Paths.Find(x => x.Destination == destination);
        float duration = path.TotalDistance / Speed;

        List<Vector3> positions = new List<Vector3>();
        positions.Add(transform.localPosition);
        path.Route.ForEach(x => positions.Add(x.transform.localPosition));

        List<float> distances = new List<float>();
        float startDistance = Vector3.Distance(transform.localPosition, currentNode.transform.localPosition);
        distances.Add(startDistance);
        path.Distances.ForEach(x => distances.Add(x));

        float totalDistance = path.TotalDistance + startDistance;

        while (timeSinceStart < duration) {
            if (isPaused) { 
                yield return new WaitForEndOfFrame();
                continue;
            }
            
            timeSinceStart += Time.deltaTime;

            float progress = timeSinceStart / duration;
            float easedProgress = EasingHelper.EaseOutSine(progress);
            
            float current = easedProgress * totalDistance;
            float distance = 0.0f;
            for (int i = 0; i < distances.Count; i++) {
                if (current > distance + distances[i]) {
                    distance += distances[i];
                    continue;
                }
                currentNode = path.Route[i];
                Vector3 from = positions[i];
                Vector3 to = positions[i + 1];
                float innerProgress = (current - distance) / distances[i];
                transform.localPosition = Vector3.Lerp(from, to, innerProgress);
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        transform.localPosition = destination.transform.localPosition;

        currentNode = destination;

        if (onReached != null) {
            onReached();
        }

    }

}
