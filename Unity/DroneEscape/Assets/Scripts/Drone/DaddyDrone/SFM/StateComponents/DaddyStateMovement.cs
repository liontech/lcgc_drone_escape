﻿using UnityEngine;
using System.Collections;

public class DaddyStateMovement : DaddyStateComponent {

    [SerializeField] private WalkNode target;
    [SerializeField] private string destinationReachedEvent;

    protected override void OnEnter() {
        GetComponentInParent<DaddyMovement>().WalkTo(target, OnDestinationReached);
    }

    private void OnDestinationReached() {
        if (!string.IsNullOrEmpty(destinationReachedEvent)) {
            GameState.TriggerEvent(destinationReachedEvent);
        }
    }

}
