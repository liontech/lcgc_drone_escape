﻿using UnityEngine;
using System.Collections;

public abstract class DaddyStateComponent : MonoBehaviour {

    protected float timeInState { get; private set; }
    protected bool isActiveState { get; private set; }

    public void Enter() {
        timeInState = 0.0f;
        isActiveState = true;
        OnEnter();
    }

    public void Exit() {
        isActiveState = false;
        OnExit();
    }

    protected virtual void OnEnter() {

    }

    protected virtual void OnExit() {

    }

    protected virtual void OnUpdate() {

    }

    private void Update() {
        if (!isActiveState) { return; }
        timeInState += Time.deltaTime;
        OnUpdate();
    }

}
