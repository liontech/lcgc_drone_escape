﻿using UnityEngine;
using System.Collections;

public class DaddyStateInteract : DaddyStateComponent {

    [SerializeField] private InteractionTrigger target;

    protected override void OnEnter() {
        target.Interaction();
    }

}
