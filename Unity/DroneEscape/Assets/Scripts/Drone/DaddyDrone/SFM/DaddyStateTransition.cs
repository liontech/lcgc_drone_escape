﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class DaddyStateTransition {

    public DaddyState TargetState { get { return targetState; } }

    [SerializeField] private string triggerName;
    [SerializeField] private List<string> conditions = new List<string>();
    [SerializeField] private DaddyState targetState;

    private DaddyStateMachine stateMachine;

    public void Initialize(DaddyStateMachine stateMachine) {
        this.stateMachine = stateMachine;

        if (!string.IsNullOrEmpty(triggerName)) {
            GameState.AddListener(triggerName, OnTrigger);
        }
    }

    private void Update() {
        if (!string.IsNullOrEmpty(triggerName)) { return; }
        OnTrigger();
    }

    private void OnTrigger() {
        if (!CheckConditions()) { return; }
        if (targetState == null) {
            Debug.LogError("Target state is null.");
            return;
        }
        stateMachine.GotoState(targetState);
    }

    private bool CheckConditions() {
        foreach (string condition in conditions) {
            if (!GameState.CheckCondition(condition)) { return false; }
        }
        return true;
    }

}
