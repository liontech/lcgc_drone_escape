﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(DaddyState))]
public class DaddyStateEditor : Editor {

    private DaddyState state;

    private void OnSceneGUI() {
        state.transform.parent.GetComponentInParent<DaddyStateMachine>().RenderInEditor();
    }

    private void Awake() {
        state = target as DaddyState;
    }

}
