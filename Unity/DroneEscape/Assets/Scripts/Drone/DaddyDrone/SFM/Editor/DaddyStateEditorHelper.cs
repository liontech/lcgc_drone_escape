﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public static class DaddyStateEditorHelper {

    public static void RenderInEditor(this DaddyStateMachine stateMachine) {
        DaddyStateMachine rootStateMachine = stateMachine.GetRootStateMachine();
        rootStateMachine.RenderStates();
    }

    private static void RenderStates(this DaddyStateMachine stateMachine) {
        Handles.color = Color.black;

        if (stateMachine.DefaultState != null) {
            Handles.DrawLine(stateMachine.transform.position, stateMachine.DefaultState.transform.position);
        }

        foreach (DaddyState state in stateMachine.GetStates()) {
            if (state.GetType() == typeof(DaddyStateMachine)) {
                (state as DaddyStateMachine).RenderStates();
            }
            foreach (DaddyStateTransition transition in state.Transitions) {
                if (transition.TargetState == null) { continue; }
                Handles.DrawLine(state.transform.position, transition.TargetState.transform.position);
            }
        }

        Handles.color = Color.white;
    }

}