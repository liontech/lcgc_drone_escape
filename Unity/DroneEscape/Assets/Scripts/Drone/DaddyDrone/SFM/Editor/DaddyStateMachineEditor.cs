﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(DaddyStateMachine))]
public class DaddyStateMachineEditor : Editor {

    private DaddyStateMachine stateMachine;

    private void OnSceneGUI() {
        stateMachine.RenderInEditor();
    }

    private void Awake() {
        stateMachine = (DaddyStateMachine)target;
    }

}
