﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DaddyState : MonoBehaviour {

    public IEnumerable<DaddyStateTransition> Transitions { get { return transitions; } }

    [SerializeField] private List<DaddyStateTransition> transitions = new List<DaddyStateTransition>();

    public virtual void Enter() {
        foreach (DaddyStateComponent component in GetComponents<DaddyStateComponent>()) {
            component.Enter();
        }
    }

    public virtual void Exit() {
        foreach (DaddyStateComponent component in GetComponents<DaddyStateComponent>()) {
            component.Exit();
        }
    }

    public DaddyStateMachine GetRootStateMachine() {
        DaddyState parentState = transform.parent.GetComponentInParent<DaddyState>();
        if (parentState != null) {
            return parentState.GetRootStateMachine();
        } else {
            return GetComponentInParent<DaddyStateMachine>();
        }
    }

    private void Awake() {
        DaddyStateMachine stateMachine = transform.parent.GetComponentInParent<DaddyStateMachine>();
        transitions.ForEach(x => x.Initialize(stateMachine));
    }

}
