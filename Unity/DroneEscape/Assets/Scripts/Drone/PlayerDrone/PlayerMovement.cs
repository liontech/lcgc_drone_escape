﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    [SerializeField] private Rigidbody myRigidbody;
    [SerializeField] private float acceleration;
    [SerializeField] private float maxSpeed;

    private void FixedUpdate() {
        if (Input.GetMouseButton(0)) {
            if (myRigidbody.velocity.magnitude < maxSpeed) {
                myRigidbody.AddForce(transform.forward * acceleration);
            }
        }
    }

}
