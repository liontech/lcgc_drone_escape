﻿using UnityEngine;
using System.Collections;

public class PlayerLook : MonoBehaviour {

    public enum RotationAxes { 
        MouseXAndY = 0, 
        MouseX = 1, 
        MouseY = 2 
    }

    public RotationAxes Axes = RotationAxes.MouseXAndY;
    public float SensitivityX = 15F;
    public float SensitivityY = 15F;
    public float MinimumX = -360F;
    public float MaximumX = 360F;
    public float MinimumY = -60F;
    public float MaximumY = 60F;

    private float rotationX = 0F;
    private float rotationY = 0F;
    private Quaternion originalRotation;

    public static float ClampAngle(float angle, float min, float max) {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

    private void Awake() {
        originalRotation = transform.localRotation;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update() {
        if (Axes == RotationAxes.MouseXAndY) {
            rotationX += Input.GetAxis("Mouse X") * SensitivityX;
            rotationY += Input.GetAxis("Mouse Y") * SensitivityY;
            rotationX = ClampAngle(rotationX, MinimumX, MaximumX);
            rotationY = ClampAngle(rotationY, MinimumY, MaximumY);
            Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
            Quaternion yQuaternion = Quaternion.AngleAxis(rotationY, -Vector3.right);
            transform.localRotation = originalRotation * xQuaternion * yQuaternion;
        } else if (Axes == RotationAxes.MouseX) {
            rotationX += Input.GetAxis("Mouse X") * SensitivityX;
            rotationX = ClampAngle(rotationX, MinimumX, MaximumX);
            Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
            transform.localRotation = originalRotation * xQuaternion;
        } else {
            rotationY += Input.GetAxis("Mouse Y") * SensitivityY;
            rotationY = ClampAngle(rotationY, MinimumY, MaximumY);
            Quaternion yQuaternion = Quaternion.AngleAxis(-rotationY, Vector3.right);
            transform.localRotation = originalRotation * yQuaternion;
        }
    }

}