﻿using UnityEngine;
using System.Collections;

public class ActivateReaction : Reaction {

	[SerializeField] private GameObject target;
    [SerializeField] private bool activate;

    protected override void React() {
        target.gameObject.SetActive(activate);
    }

}
