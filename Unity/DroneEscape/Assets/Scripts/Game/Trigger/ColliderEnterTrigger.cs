﻿using UnityEngine;
using System.Collections;

public class ColliderEnterTrigger : EventTrigger {

    private void OnTriggerEnter(Collider collider) {
        Trigger();
    }

}
