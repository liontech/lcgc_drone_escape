﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public static class GameState {

    private static readonly Dictionary<string, List<Action>> events = new Dictionary<string,List<Action>>();
    private static readonly Dictionary<string, bool> conditions = new Dictionary<string,bool>();
    
    public static void Reset() {
        events.Clear();
        conditions.Clear();
    }

    public static void AddListener(string eventName, Action onEvent) {
        AddEventIfNeeded(eventName);
        events[eventName].Add(onEvent);
    }

    public static void TriggerEvent(string eventName) {
        AddEventIfNeeded(eventName);
        events[eventName].ForEach(x => x());
    }

    public static bool CheckCondition(string conditionName) {
        AddConditionIfNeeded(conditionName);
        return conditions[conditionName];
    }

    private static void AddEventIfNeeded(string eventName) {
        if (events.ContainsKey(eventName)) { return; }
        events.Add(eventName, new List<Action>());
    }

    private static void AddConditionIfNeeded(string conditionName) {
        if (conditions.ContainsKey(conditionName)) { return; }
        conditions.Add(conditionName, false);
    }

}
