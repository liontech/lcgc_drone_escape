﻿using UnityEngine;
using System.Collections;
using System;

public class CoroutineHelper : MonoBehaviour {

    static CoroutineHelper instance;

	public static void Delay(Action action, float delay) {
        GetOrCreateInstance().StartCoroutine("InstanceDelay", new object[] { action, delay });
    }

    private static CoroutineHelper GetOrCreateInstance() {
        if (instance == null) {
            instance = (new GameObject("CoroutineHelper")).AddComponent<CoroutineHelper>();
        }
        return instance;
    }

    private IEnumerator InstanceDelay(object[] parameters) {
        Action action = parameters[0] as Action;
        float delay = (float)parameters[1];
        yield return new WaitForSeconds(delay);
        action();
    }

}
