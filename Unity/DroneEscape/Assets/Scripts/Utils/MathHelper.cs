﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MathHelper {

    public static Vector2 GetLineIntersection(Vector2 start1, Vector2 end1, Vector2 start2, Vector2 end2) {
        
        float d = (start1.x - end1.x) * (start2.y - end2.y) - (start1.y - end1.y) * (start2.x - end2.x);
        if (d == 0) { return Vector2.zero; }

        float xi = ((start2.x - end2.x) * (start1.x * end1.y - start1.y * end1.x) - (start1.x - end1.x) * (start2.x * end2.y - start2.y * end2.x)) / d;
        float yi = ((start2.y - end2.y) * (start1.x * end1.y - start1.y * end1.x) - (start1.y - end1.y) * (start2.x * end2.y - start2.y * end2.x)) / d;

        return new Vector2(xi, yi);
    }

    public static bool IsIntersecting(Vector2 a, Vector2 b, Vector2 c, Vector2 d) {
        if (a == c) { return false; }
        if (a == d) { return false; }
        if (b == c) { return false; }
        if (b == d) { return false; }

        float denominator = ((b.x - a.x) * (d.y - c.y)) - ((b.y - a.y) * (d.x - c.x));
        float numerator1 = ((a.y - c.y) * (d.x - c.x)) - ((a.x - c.x) * (d.y - c.y));
        float numerator2 = ((a.y - c.y) * (b.x - a.x)) - ((a.x - c.x) * (b.y - a.y));

        bool isIntersecting = false;

        if (denominator == 0) {
            isIntersecting = numerator1 == 0 && numerator2 == 0;
        } else {

            float r = numerator1 / denominator;
            float s = numerator2 / denominator;

            isIntersecting = (r >= 0 && r <= 1) && (s >= 0 && s <= 1);
        }

        if (isIntersecting) {
            Debug.DrawLine(new Vector3(a.x, 2.0f, a.y), new Vector3(b.x, 2.0f, b.y), Color.red);
            Debug.DrawLine(new Vector3(c.x, 2.0f, c.y), new Vector3(d.x, 2.0f, d.y), Color.red);
        }

        return isIntersecting;
    }

    public static Vector3 ToFlatVector3(this Vector2 vector) {
        return new Vector3(vector.x, 0.0f, vector.y);
    }
    
    public static Vector2 ToFlatVector2(this Vector3 vector) {
        return new Vector2(vector.x, vector.z);
    }

    public static Vector3 Flattened(this Vector3 vector) {
        vector.y = 0.0f;
        return vector;
    }

    public static Vector3 SetXZ(Vector3 original, Vector3 value) {
        original.x = value.x;
        original.z = value.z;
        return original;
    }

    public static bool FlipCoin() {
        return Random.value > .5f;
    }

    public static bool RoleDice(int sideCount) {
        return Random.Range(0, sideCount) == 0;
    }

    public static Quaternion Randomize(this Quaternion quaternion) {
        quaternion.eulerAngles = new Vector3(Random.value * 360.0f, Random.value * 360.0f, Random.value * 360.0f);
        return quaternion;
    }

    public static Vector3 RandomVector3() {
        return new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
    }

    public static void CalcOuterPositions(List<Vector2> positions, float expandOffset, out Vector2 topLeft, out Vector2 bottomRight) {
        if (positions.Count == 0) {
            topLeft = -Vector2.one * expandOffset;
            bottomRight = Vector2.one * expandOffset;
            return;
        }

        topLeft = positions[0];
        bottomRight = positions[0];

        foreach (Vector2 position in positions) {
            topLeft.x = Mathf.Min(topLeft.x, position.x);
            topLeft.y = Mathf.Min(topLeft.y, position.y);
            bottomRight.x = Mathf.Max(bottomRight.x, position.x);
            bottomRight.y = Mathf.Max(bottomRight.y, position.y);
        }

        topLeft -= Vector2.one * expandOffset;
        bottomRight += Vector2.one * expandOffset;
    }

    public static Vector2 Floor(this Vector2 vector) {
        return new Vector2(Mathf.Floor(vector.x), Mathf.Floor(vector.y));
    }

    public static Vector2 Ceil(this Vector2 vector) {
        return new Vector2(Mathf.Ceil(vector.x), Mathf.Floor(vector.y));
    }

    public static float UnclampedLerp(float from, float to, float progress) {
        return from + (to - from) * progress;
    }

}