﻿using UnityEngine;
using System.Collections;

public static class CameraHelper {

	public static Vector3 ScreenToFloorPlane(this Camera camera, Vector2 screenPosition) {
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        Ray ray = camera.ScreenPointToRay(screenPosition);
        float distance;
        if (plane.Raycast(ray, out distance)) {
            return ray.GetPoint(distance);
        }
        return Vector3.zero;
    }

}
