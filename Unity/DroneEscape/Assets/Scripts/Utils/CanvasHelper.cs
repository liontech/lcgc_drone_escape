﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public static class CanvasHelper {
    
    public static Vector3 WorldToCanvasPosition(this Canvas canvas, Vector3 worldPosition) {
        Vector2 screenPosition = RectTransformUtility.WorldToScreenPoint(Camera.main, worldPosition);
        return canvas.ScreenToCanvasPosition(screenPosition);
    }

    public static Vector2 CanvasToScreenPosition(this Canvas canvas, Vector3 canvasPosition) {
        return RectTransformUtility.WorldToScreenPoint(null, canvasPosition);
    }

    public static Vector3 ScreenToCanvasPosition(this Canvas canvas, Vector2 screenPosition) {
        RectTransform canvasRect = canvas.GetComponent<RectTransform>();
        Vector3 positionInCanvas;
        RectTransformUtility.ScreenPointToWorldPointInRectangle(canvasRect, screenPosition, canvas.worldCamera, out positionInCanvas);
        return positionInCanvas;
    }

    public static bool IsMouseOverUI() {
        return EventSystem.current.IsPointerOverGameObject();
    }

}
